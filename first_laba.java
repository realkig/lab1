// работает на jvm 8 
import java.util.concurrent.ThreadLocalRandom;
import java.util.Random;

public class first_laba {
    // сортировка quickSort (быстрая сортировка)
    public static void quickSort(int [][] numbers){
        for (int [] k: numbers) quickSort(k, 0, k.length - 1);
    }
    public static void quickSort(int [] numbers){
        quickSort(numbers, 0, numbers.length - 1);
    }
    public static void quickSort(int [] numbers, int l, int h) {
        if (l>=h) return;
        //завершить выполнение если границы массива numbers (l, h)
        //совпадают или h > l
        // l - нижняя граница
        // h - верхняя граница
        
        int temp;
        // выбрать опорный элемент p
        int m = l + (h - l) / 2;
        int p = numbers[m];
        // разделить на подмассивы, элементы которых больше и мешьше p
        int i = l, j = h;
        while (i <= j) {
            while (numbers[i] < p) {
                i++;
            }
            while (numbers[j] > p) {
                j--;
            }

            if (i <= j) {
            // меняем местами элементы с индексами i и j
                temp = numbers[i];
                numbers[i] = numbers[j];
                numbers[j] = temp;
                i++;
                j--;
            }
        }
        // сортировать левые и правые части
        quickSort(numbers, l, j);
        quickSort(numbers, i, h);
    }
    
    // cортировка выборкой
    public static void selectionSort (int[][] numbers){
        for (int [] k: numbers) selectionSort(k);
    }
    public static void selectionSort (int[] numbers){
        int index_of_min, temporary;
        for (int index = 0; index < numbers.length-1; index++){
            // установить начальное значение минимального индекса
            index_of_min = index;
            // найти индекс минимального элемента 
            for (int scan = index+1; scan < numbers.length; scan++){
                if (numbers[scan] < numbers[index_of_min]) index_of_min = scan;
            }
            // поменять значения 
            temporary = numbers[index_of_min];
            numbers[index_of_min] = numbers[index];
            numbers[index] = temporary;
        };
    };
    
    // сортировка вставкой
    public static void insertionSort (int[][] numbers){
        for (int [] k: numbers) insertionSort(k);
    }
    public static void insertionSort( int[] numbers ){
        int j = 1;
        int i;
        int key;
        // цикл, выполнить до тех пор, пока index < размера массива
        while (j < numbers.length){
            i = j - 1; 
            key = numbers[j]; 
            
            while (numbers[i]>key ){
                // выполнить до тех пор, пока i>=0
                // и numbers[i] > key
                numbers[i+1] = numbers[i];
                i -= 1;
                if (i<0) break; 
            }
            numbers[i+1] = key;
            j +=1;
        }
    };
    
    // сортировка обменом (пузырьком)
    public static void bubbleSort (int[][] numbers){
        for (int [] k: numbers) bubbleSort(k);
    }
    public static void bubbleSort( int [] numbers){
        int n = numbers.length - 1;
        int j;
        int temp1; // 
        int temp2; //
        boolean F;  // флаг, показывающий
        // текущее состояние сортировки
        
        while (true){
            F = true;
            for (j = 0; j<n; j++){ 
                
                temp1 = numbers[j];
                temp2 = numbers[j+1];
                
                if (temp1 > temp2){
                    // если текущий элемент 
                    // меньше следующего, то поменять 
                    // местами
                    numbers[j] = temp2;
                    numbers[j+1] = temp1;
                    F = false; 
                }
            }
            if (F) break; // если перестановки не было, то завершить
        } 
    };
    
    // дополнительный материал из
    // http://algolist.manual.ru/sort/shell_sort.php
    // сортировка Шелла
    public static void shellSort (int[][] numbers){
        for (int [] k: numbers) shellSort(k);
    }
    public static void shellSort(int [] numbers){
        int len = numbers.length; // размер массива
        int temp, s, i, j;        
        
        for(s = len/2; s > 0; s/=2){
            
            for(i=0; i<len; i++){
                
                for(j=i+s; j<len; j+=s){
                
                    if(numbers[i] > numbers[j]){
                        temp = numbers[j];
                        numbers[j] = numbers[i];
                        numbers[i] = temp;
                    }
                }
            }
        }
    };
    
    // дополнительный материал из 
    // https://studref.com/370980/informatika/turnirnaya_sortirovka
    // турнирная сортировка
    public static void tourSort (int[][] numbers){
        for (int [] k: numbers) tourSort(k);
    }
    public static void tourSort(int [] numbers){
        int i;
        // объявить массив чисел "Участников"
        Integer[]participants = new Integer[numbers.length];
        for (i = 0; i<numbers.length; i++){
            // заполнить массив "Участников"
            participants[i] = numbers[i];
        }
        for (i = 0; i<numbers.length; i++){
            // распределить числа 
            numbers[i] = tour(participants);
        }
    }
    
    // находит число "победитель" из массива чисел "участников"
    private static int tour(Integer [] numbers){
        int i, len, j, a, b;
        len = numbers.length; // размер массива "участников"
        int [] indexes; // массив индексов
        int [] temp1, temp2;
        
        indexes = new int[numbers.length];//cоздать массив из
        // индексов не null-евых "участников"
        j = 0;
        for (i=0; i<len; i++){
            if (numbers[i]!=null){
                indexes[j]=i;
                j+=1;
            }
        }
        len = j; // размер массива "участников"
        
        temp1 = new int[numbers.length]; // новый массив из "призеров"
        
        while (len > 1){ // повторить, пока размер массива "призеров" > 1
            if ((len%2)==1){
                j=1;         // если массив не кратен 2, то 
                i=1;         // добавить первое число в массив "призеров"
                temp1[0]=indexes[0];
            }
            else{
                j=0;
                i=0;
            }
            while (i<len){ // наполнить массив "призеров"
                a = indexes[i];
                i +=1;
                b = indexes[i];
                i +=1;
                if (numbers[a]>numbers[b]){
                    a = b;
                }
                temp1[j] = a; // добавить "призера"
                j+=1;
            }
            len = j;
            temp2 = indexes;  
            indexes = temp1; 
            temp1 = temp2;
        }
        a = indexes[0];
        b = numbers[a];
        numbers[a] = null;
        return b;
    }
    
    // пирамидальная сортировка
    public static void heapSort (int[][] numbers){
        for (int [] k: numbers) heapSort(k);
    }
    public static void heapSort(int [] numbers){
        int temp;
        int n = numbers.length; // размер массива
        int i = n/2-1;
        // построение кучи 
        while (i >= 0){
            heapify(numbers, n, i);
            i-=1;
        }
        i=n-1;
        // извлекаем элементы из кучи
        while(i>=0){
            // перемещаем текущий корень в конец
            temp = numbers[0];
            numbers[0] = numbers[i];
            numbers[i] = temp;
            // вызываем процедуру heapify на уменьшенной куче
            heapify(numbers, i, 0);
            i-=1;
        }
    }
    
    
    // сформировать кучу из поддерева с корнем i
    // i - индекс в numbers
    public static void heapify(int [] numbers, int n, int i){
        int temp;
        int b = i; // наибольший элемент
        int l = (i+i)+1; // левый дочерний 
        int r = l+1;// правый дочерний
        if (l<n) if (numbers[l] > numbers[b]){
            b = l; // если элемент с индексом l больше элементa с индексом b
        };
        if (r<n) if (numbers[r] > numbers[b]){
            b = r; // если элемент с индексом r больше элементa с индексом b
        };
        //если корень не наибольший элемент
        if (b != i){
           // поменять корень и наибольший элемент местами
           temp=numbers[i];
           numbers[i]=numbers[b];
           numbers[b]=temp;
           
           // преобразовать в двоичную кучу поддерево b
           heapify(numbers, n, b);
        }
    }
    
    // функция выводит на экран скорость работы метода сортировки
    public static void getTime(int [][] numbers, Sort sort, String name){
        int [][] k = new int[numbers.length][];
        for (int i = 0; i<numbers.length; i++){
            k[i] = numbers[i].clone();
        }
        System.out.println(name);
        System.out.println("Матрица до обработки:");
        for (int[] t: k) {
            for (int i: t) System.out.print(" "+i);
            System.out.println();
        }           
        long time = System.nanoTime();
        sort.sort(k);
        time = System.nanoTime() - time;
        System.out.println("\nМатрица после обработки:");
        for (int[] t: k) {
            for (int i: t) System.out.print(" "+i);
            System.out.println();
        }    
        System.out.println("\nВремя работы в наносекундах: " + time+"\n");  
    }
    
    // функция рандомно перемешивает элементы в массиве
    static int[] shuffleArray(int[] ar){
        Random rnd = ThreadLocalRandom.current();
        for (int i = ar.length - 1; i > 0; i--)
        {
          int index = rnd.nextInt(i + 1);
          int a = ar[index];
          ar[index] = ar[i];
          ar[i] = a;
        }
        return ar;
    }
    
    
    public static void main(int [][] k){
        getTime (k, (z)->quickSort(z), "quickSort");
        getTime (k, (z)->selectionSort(z), "selectionSort");
        getTime (k, (z)->insertionSort(z), "insertionSort");
        getTime (k, (z)->bubbleSort(z), "bubbleSort");
        getTime (k, (z)->shellSort(z), "shellSort");
        getTime (k, (z)->tourSort(z), "tourSort");
        getTime (k, (z)->heapSort(z), "heapSort");
    }
    public static void main(String [] argv){
        main(new int[][] {
         shuffleArray(new int[] {1,7,13,19,25}),
         shuffleArray(new int[] {2,8,14,20,26}),
         shuffleArray(new int[] {3,9,15,21,27}),
         shuffleArray(new int[] {4,10,16,22,28}),
         shuffleArray(new int[] {5,11,17,23,29}),
         shuffleArray(new int[] {6,12,18,24,30})});
    }
}

interface Sort{
    public void sort(int [][] numbers);
}
